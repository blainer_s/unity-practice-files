﻿using UnityEngine;
using System.Collections;

public class SwirlyWithFriction : MonoBehaviour {

	float x_counter = 0;
	float z_counter = 0;
	float x_time = .01f;
	float z_time = .0183f;

	float speed = 1;
	float friction = .99f;

public float glowRate = 2f;

	// Use this for initialization
	void Start () {
		x_counter = Random.Range(-3, 3);
		z_counter = Random.Range(-3, 3);
		light.color = new Color( Random.value, Random.value, Random.value, 1f);
	}
	
	// Update is called once per frame
	void Update () {
		x_counter += x_time * speed;
		z_counter += z_time * speed;
		//add friction
		speed *= friction;

		transform.position = new Vector3(
			Mathf.Sin(x_counter) * 20,
			0f,
			Mathf.Sin(z_counter) * 15
		);

		Glow();
	}
	public void addSpeed(float sTA) {
		speed+= sTA;
	}
	void Glow() {
		light.intensity = Mathf.Sin(Time.time * glowRate) * .5f + .5f;
	}
}

