﻿using UnityEngine;
using System.Collections;

public class testScript : MonoBehaviour {
	
	public GameObject explosion;
	void OnCollisionEnter(Collision other) {
		if (other.gameObject.CompareTag("Bullet")) { 
			Destroy (other.gameObject);
			Instantiate(explosion, transform.position, transform.rotation);
		}
		this.rigidbody.useGravity = true;
		this.light.enabled = true;
		rigidbody.AddForce(Vector3.down * 100f);
	}
}
