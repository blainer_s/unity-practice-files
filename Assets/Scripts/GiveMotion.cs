﻿using UnityEngine;
using System.Collections;

public class GiveMotion : MonoBehaviour {

	SwirlyWithFriction[] swfArray;

	int mailBoxNumber = 10;
	int[] mailboxNumbers = { 2, 23, 405, 40, 20, 201 };

	void Start() {
		swfArray = GetComponentsInChildren<SwirlyWithFriction>();
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.A) ) {
			BroadcastMessage("addSpeed", 20f);
		}
		if (Input.GetKeyDown(KeyCode.D) ) {
			BroadcastMessage("addSpeed", -2f);
		}
		if (Input.GetKeyDown(KeyCode.F)) {
			//take each SwirlyWithFriction out of the array swfArray
			//name that SwirlyWithFriction "s"

			for (int i = 0; i<swfArray.Length; i++) {
				swfArray[i].addSpeed(4f);
			}
			//foreach(int aMailBox in mailboxNumbers) {}

			/*foreach( SwirlyWithFriction s in swfArray) {
				s.addSpeed(4f);
			}*/
		}
	}
}








