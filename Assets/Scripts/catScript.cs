﻿using UnityEngine;
using System.Collections;

public class catScript : MonoBehaviour {

	public float topRotation;
	
	// Update is called once per frame
	void Update () {
		transform.rotation = Quaternion.identity;
	}

	void OnMouseDown() {
		SendMessageUpwards("rotateTo", Quaternion.Euler(0, 0, topRotation));
	}
}
