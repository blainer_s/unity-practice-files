﻿ using UnityEngine;
using System.Collections;

public class heylookoverhere : MonoBehaviour {

	public Transform cap1, cap2, cap3;

	Vector3 lastPos = Vector3.zero;
	Vector3 nextPos = Vector3.zero;
	float percentage = 0;

	void Start () {
		cap1 = GameObject.Find("Capsule1").transform;
		cap2 = GameObject.Find("Capsule2").transform;
		cap3 = GameObject.Find("Capsule3").transform;
	}
	
	void Update () {

		percentage += Time.deltaTime;

		transform.LookAt( Vector3.Lerp(lastPos, nextPos, percentage) );

		//if the 1 key is pressed
		if (Input.GetKeyDown ("1") ) {
			lastPos = Vector3.Lerp(lastPos, nextPos, percentage);
			nextPos = cap1.position; //move the camera to face the first capsule
			percentage = 0;
		}
		if (Input.GetKeyDown ("2") ) {
			lastPos = Vector3.Lerp(lastPos, nextPos, percentage);
			nextPos = cap2.position; //move the camera to face the first capsule
			percentage = 0; //move the camera to face the first capsule
		}
		if (Input.GetKeyDown ("3") ) {
			lastPos = Vector3.Lerp(lastPos, nextPos, percentage);
			nextPos = cap3.position; //move the camera to face the first capsule
			percentage = 0; //move the camera to face the first capsule
		}
	}
}
