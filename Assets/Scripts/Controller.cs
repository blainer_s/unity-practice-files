﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {

	Ray mouseDir;
	// Update is called once per frame
	void Update () {
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");

		transform.position = new Vector3 (transform.position.x + Time.deltaTime * 10 * h, 2, transform.position.z + Time.deltaTime * 10 * v);

		if (Input.GetMouseButtonDown(0) ) {
			RaycastHit whatDidIHit;
			mouseDir = Camera.main.ScreenPointToRay( Input.mousePosition );
		
			if (Physics.Raycast( mouseDir.origin, mouseDir.direction, out whatDidIHit, 10) ) {
				Debug.Log("howdy howdy howdy");
				whatDidIHit.transform.gameObject.SetActive(false);
			}
		}
	}

	void OnDrawGizmos() {
		if (Input.GetMouseButton(0) ) {
			Gizmos.color = Color.cyan;
			Gizmos.DrawRay(transform.position, mouseDir.direction * 10);
		}
	}
}
