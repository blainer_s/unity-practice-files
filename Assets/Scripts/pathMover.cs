﻿using UnityEngine;
using System.Collections;

public class pathMover : MonoBehaviour {

	float speed = 8f;
	Rigidbody rbody;
	Transform otherObject;

	string State = "Forward";
	
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (State == "Forward") {
			//move forward State == forward
			rigidbody.velocity = Vector3.forward * speed;
		} else if (State == "Right") {
			rigidbody.velocity = Vector3.right * speed;
		}else if (State == "Back") {
			rigidbody.velocity = Vector3.back * speed;
		}else if (State == "Left") {
			rigidbody.velocity = Vector3.left * speed;
		}
	}
	void OnCollisionEnter(Collision barrier) {
		growBigger gB = barrier.gameObject.GetComponent<growBigger>();
		if (gB != null) {
			gB.growTaller();
		}

		if (State == "Forward") {
			State = "Right";
		} else if (State == "Right") {
			State = "Back";
		} else if (State == "Back") {
			State = "Left";
		} else if (State == "Left") {
			State = "Forward";
		}
	}


}