﻿using UnityEngine;
using System.Collections;

public class coRoutine : MonoBehaviour {

	public Transform start;
	public Transform end;
	public float position = .5f; 

	// Update is called once per frame
	void Update () {
		position -= Time.deltaTime/2;
		if (position > 1) {
			position = 0;
		}
		if (position < 0) {
			position = 1;
		}
		transform.position = Vector3.Lerp(start.position, end.position, position);
		renderer.material.color = Color.Lerp( Color.red, Color.blue, position);
		transform.rotation = Quaternion.Slerp(Quaternion.identity, new Quaternion(0, 1, 1, 0), position);
	}
}
