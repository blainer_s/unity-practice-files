﻿using UnityEngine;
using System.Collections;

public class moveUpAgain : MonoBehaviour {
	float startY  = 0;

	float speedY = 2f;

	void Start() {
		startY = transform.position.y;
	}
	void Update () {
		if (transform.position.y > 8f + startY) {
			speedY *= -1;
		}
//			transform.position += Vector3.up * 2f * Time.deltaTime;


		if (transform.position.y < startY) {
			speedY *= -1;
		}
	}
	void FixedUpdate() {
		rigidbody.velocity = Vector3.up * speedY;
	}
}
