﻿using UnityEngine;
using System.Collections;

public class lerpers : MonoBehaviour {

	public Transform from;
	public Transform to;

	bool isGoingForwards = true;

	int direction = 1;

	public float percentage = 0f;
	void Update () {

		percentage += direction * (Time.deltaTime/2f); //takes 2 seconds
		
		if (percentage > 1) { //if it's at the end
			//go backwards
			direction = -1;
		}
		if (percentage < 0) { //if it's at the beginning
			direction = 1;
		}
		
		//Vector3 Lerp(Vector3 from, Vector3 to, float t);
		transform.position = Vector3.Lerp( from.position, to.position, percentage);
		renderer.material.color = Color.Lerp(Color.red, Color.blue, percentage);
		transform.rotation = Quaternion.Slerp(Quaternion.identity, new Quaternion(1, 0, 1, 0), percentage);
	}


	void Old () {
		// if going backwards
		if ( isGoingForwards == false ) {
			percentage -= Time.deltaTime/2f; //takes 2 seconds
		} else {
			percentage += Time.deltaTime/2f; //takes 2 seconds
		}

		if (percentage > 1) { //if it's at the end
			//go backwards
			isGoingForwards = false;
		}
		if (percentage < 0) { //if it's at the beginning
			isGoingForwards = true; //go forwards
		}

		//Vector3 Lerp(Vector3 from, Vector3 to, float t);
		transform.position = Vector3.Lerp( from.position, to.position, percentage);
		renderer.material.color = Color.Lerp(Color.red, Color.blue, percentage);
		transform.rotation = Quaternion.Slerp(Quaternion.identity, new Quaternion(1, 0, 1, 0), percentage);
	}

}





