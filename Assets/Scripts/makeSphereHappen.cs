﻿using UnityEngine;
using System.Collections;

public class makeSphereHappen : MonoBehaviour {

	public GameObject sphere;

	void Start () {	
	}

	int repeatAmount = 0;
	int maxRepeat = 5;

	void doSphereSpawn() {
		Instantiate(sphere, transform.position, transform.rotation);	
		repeatAmount += 1;
		if (repeatAmount >= maxRepeat) {
			stopRepeating();
		}
	}

	void stopRepeating() {
		CancelInvoke("doSphereSpawn");
		repeatAmount = 0;
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.K) ) {
			InvokeRepeating ( "doSphereSpawn", 0f, 2f);
		}
	}
}
