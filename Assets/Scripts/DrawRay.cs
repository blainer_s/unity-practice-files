﻿using UnityEngine;
using System.Collections;

public class DrawRay : MonoBehaviour {
	Ray myFirstRay;
	public float distance = 10f;
	void Start() {
		myFirstRay = new Ray(Vector3.zero, Random.insideUnitCircle);
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.red;
//		Gizmos.DrawRay( myFirstRay.origin, myFirstRay.direction );
		Gizmos.DrawRay( myFirstRay.origin, transform.forward * distance );

		//Raycast(Vector3 origin, Vector3 direction, float distance = Mathf.Infinity);
		if ( Physics.Raycast(myFirstRay.origin, transform.forward, distance ) ) {
			Debug.Log("howdy howdy howdy");
		}
	}
}
