﻿using UnityEngine;
using System.Collections;

public class doAndWait : MonoBehaviour {
	void OnGUI() {
		if (GUI.Button( new Rect( Screen.width - 210, 10, 200, 20), "Do A List of Things") ) {
			StartCoroutine("CommandList");
		}
	}

	IEnumerator CommandList() {
		Debug.Log("howdy howdy");
		yield return new WaitForSeconds(2);
		Debug.Log("again again");

		while (transform.position.z < 10) {
			yield return null;
			transform.position += Vector3.forward * Time.deltaTime * 8;
			Debug.Log(transform.position.z);
		}

		yield return new WaitForSeconds(2f);
		Debug.Log("after move");
	}
}
