﻿using UnityEngine;
using System.Collections;

public class whatalooker : MonoBehaviour {

	public Transform cube1;
	public Transform cube2;
	public Transform cube3;

	Vector3 target, old_target;
	float position = 0f;
	// Use this for initialization
	void Start () {
		old_target = Vector3.zero;
		cube1 = GameObject.Find("Cube1").transform;
		cube2 = GameObject.Find("Cube2").transform;
		cube3 = GameObject.Find("Cube3").transform;
	}
	
	// Update is called once per frame
	void Update () {
		position += Time.deltaTime;
		transform.LookAt(Vector3.Lerp(old_target, target, position));

		if (Input.GetKeyDown("1") ) {
			old_target = Vector3.Lerp(old_target, target, position);
			target = cube1.position;
			position = 0;
		}
		if (Input.GetKeyDown("2") ) {
			old_target = Vector3.Lerp(old_target, target, position);
			target = cube2.position;
			position = 0;
		}
		if (Input.GetKeyDown("3") ) {
			old_target = Vector3.Lerp(old_target, target, position);
			target = cube3.position;
			position = 0;
		}
	}
}
