﻿using UnityEngine;
using System.Collections;

public class charMover : MonoBehaviour {

	public float speed = 10f;
	Transform other; 
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float horz = Input.GetAxis("Horizontal");
		float vert = Input.GetAxis("Vertical");

		transform.position = new Vector3( 
		                                 transform.position.x + horz * Time.deltaTime * speed, 
		                                 transform.position.y, 
		                                 transform.position.z + vert * Time.deltaTime * speed 
		                                 );
		if (Input.GetMouseButtonDown(0) ) {
			//	camera.ScreenPointToRay(new Vector3(200, 200, 0));
			Ray mouseClick = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit infoAboutCollision; 
			if (Physics.Raycast( mouseClick.origin, mouseClick.direction, out infoAboutCollision ) ) {
				Debug.Log("there is something in front of you");
				infoAboutCollision.transform.parent = this.transform;
			}
		}
	}

}








