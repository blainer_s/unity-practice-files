﻿using UnityEngine;
using System.Collections;

public class drawMenu : MonoBehaviour {

	public GameObject toMake;

	void makeThings() {
		for (int i = 0; i<100; i++) {
			GameObject go = Instantiate(toMake, Random.insideUnitSphere * 10f, Random.rotation) as GameObject;
			go.rigidbody.AddForce(Random.insideUnitSphere * 1000);
		}
	}

	void moveBulls() {
		GameObject[] obs = GameObject.FindGameObjectsWithTag("bullets");
		foreach( GameObject go in obs) {
			go.rigidbody.AddForce(go.transform.forward * 100);
		}
	}

	void OnGUI() {
		if (GUI.Button( new Rect(20, 20, 400, 30), "Load Level 1")) {
			//Application.LoadLevel("In Class");
//			makeThings();
			moveBulls();
		}
		if (GUI.Button( new Rect(20, 60, 400, 30), "Load Level 2")) {
			//Application.LoadLevel("cube move Example");
			//StartCoroutine("coTine");
			makeThings();
		}
		if (GUI.Button( new Rect(20, 100, 400, 30), "Restart")) {
			Application.LoadLevel(Application.loadedLevelName);
		}
	}
	IEnumerator coTine() {
		Debug.Log("message 1");
		yield return new WaitForSeconds(2);
		while ( transform.position.z < 10 ) {
			yield return null;
			transform.position += transform.forward * Time.deltaTime * 8;
		}
		Debug.Log("another");
	}
}
