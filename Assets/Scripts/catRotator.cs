﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class catRotator : MonoBehaviour {
	List<Transform> cats = new List<Transform>();
	float dist = 3f;
	// Use this for initialization
	void Start () {
		/*foreach (Transform t in GetComponentsInChildren<Transform>()) {
			if (t != transform) {
				cats.Add(t);
			}
		}
		Debug.Log(cats.Count);
		float angle = Mathf.PI*2f/cats.Count;
		float cur_angle = 0f;
		foreach (Transform t in cats) {
			t.position = new Vector3(Mathf.Cos(cur_angle) * dist, Mathf.Sin(cur_angle) * dist, 0);
			cur_angle += angle;
		}*/
		transform.rotation = Quaternion.identity;
	}
	public void rotateTo(Quaternion to) {
		StopAllCoroutines();
		StartCoroutine("Rotate", to);
	}

	IEnumerator Rotate(Quaternion to) {
		while (transform.rotation != to) {
			transform.rotation = Quaternion.RotateTowards(transform.rotation, to, 1f);
			yield return null;
		}
	}
}
