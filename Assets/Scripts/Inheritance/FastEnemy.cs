﻿using UnityEngine;
using System.Collections;

public class FastEnemy : Enemy_base {
	
	float speed = 25f;
	void Start () {
		Invoke("TurnWaitGo", 2f);
	}
	public override void Update() {
		transform.position += transform.forward * speed * Time.deltaTime;

		base.Update();
	}
	void TurnWaitGo() {
		transform.Rotate(Vector3.up * Random.Range(-180f, 180f));
		Invoke("TurnWaitGo", Random.Range(1.5f, 3.5f));
	}	
}
