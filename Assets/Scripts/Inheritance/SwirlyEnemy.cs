﻿using UnityEngine;
using System.Collections;

public class SwirlyEnemy : Enemy_base {
	Transform initPosition;

	float x_counter = 0;
	float z_counter = 0;
	float x_time = .01f;
	float z_time = .0183f;

	void Start () {
		initPosition = transform;
		light.color = new Color(1F, 0.3F, 0.4F, 1F);
		glowRate = 3f;
	}
	
	public override void Update () {
		
		x_counter += x_time;
		z_counter += z_time;

		transform.position = new Vector3(
			Mathf.Sin(x_counter) * 38,
			initPosition.position.y,
			Mathf.Sin(z_counter) * 30
		);
		Glow();
	}
}
