﻿using UnityEngine;
using System.Collections;

public class Enemy_base : MonoBehaviour {
	
	int HitPoints = 200;
	public float glowRate = 2f;

	public virtual void Update () {
		Glow();
		WrapScreen();
	}

	public virtual void Glow() {
		light.intensity = Mathf.Sin(Time.time * glowRate) * .5f + .5f;
	}
	void WrapScreen() {
		//Wrap Screen
		//I'm using magic numbers for screen bounds on a 4:3 screen.
		//yes there is a better way of doing this, but it takes longer
		Vector3 ViewportPoint = Camera.main.WorldToViewportPoint(transform.position);
		if (ViewportPoint.y < 0) {
			transform.position = new Vector3(transform.position.x,transform.position.y,28);
		} else if (ViewportPoint.y > 1) {
			transform.position = new Vector3(transform.position.x, transform.position.y, -24);
		} else if (ViewportPoint.x < 0) {
			transform.position = new Vector3(38, transform.position.y, transform.position.z);
		}else if (ViewportPoint.x > 1) {
			transform.position = new Vector3(-31, transform.position.y, transform.position.z);
		} 
	}
}
