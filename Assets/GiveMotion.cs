﻿using UnityEngine;
using System.Collections;

public class GiveMotion : MonoBehaviour {

	SwirlyWithFriction[] swfArray; //an array to hold the scripts

	void Start() {
		swfArray = GetComponentsInChildren<SwirlyWithFriction>();
		//assign all scripts to the array. 
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.A) ) {
			BroadcastMessage("addSpeed", 20f);
		}
		if (Input.GetKeyDown(KeyCode.D) ) {
			BroadcastMessage("addSpeed", -2f);
		}
		if (Input.GetKeyDown(KeyCode.F)) {
			for (int i = 0; i<swfArray.Length; i++) {
				swfArray[i].addSpeed(4f);
				//this for loop does the same job as the foreach below
			}
			//take each SwirlyWithFriction out of the array swfArray
			//name that SwirlyWithFriction "s"
			/*foreach( SwirlyWithFriction s in swfArray) {
				s.addSpeed(4f);
			}*/
		}
	}
}
